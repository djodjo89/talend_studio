# talend_studio
## description
simple project to run Talend Studio locally with Docker
## install project (only do it once)
```sh
make all
```
## run
(not very standard, but practical)
```sh
make
```
## 🚨 do it before opening TOS interface, access to database
run project, then go to `localhost:8082` and connect (id is "sa")
