.PHONY: run download_and_extract env all
run:
	xhost +
	docker compose up --build --remove-orphans
download_and_extract:
ifneq ("$(wildcard tol.zip)", "")
	wget 'https://filesender.renater.fr/download.php?token=db0f8bb9-391f-4f5a-9b6b-f9a7e00542f1&files_ids=35494802'
	mv 'download.php?token=db0f8bb9-391f-4f5a-9b6b-f9a7e00542f1&files_ids=35494802' tol.zip
	unzip -d tol tol.zip
	mv tol/* .
endif
env:
	cp .env.dev .env
all:
	make download_and_extract
	make env
	make run
